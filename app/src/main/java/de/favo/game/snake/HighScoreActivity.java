package de.favo.game.snake;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class HighScoreActivity extends Activity {
    private List<HighScoreManager.HighScoreEntry> highScoreEntryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Adapter mAdapter;
    private HighScoreManager mgr;
    private OnSwipeTouchListener onSwipeTouchListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        recyclerView =  findViewById(R.id.recycler_view);
        mgr = new HighScoreManager(this);
        highScoreEntryList = mgr.getEntriesSortedByBest();
        mAdapter = new Adapter(this, highScoreEntryList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        ((TextView)findViewById(R.id.highscore_act_title)).setTypeface(GameHelpers.getBoldFont(this));
        ((TextView)findViewById(R.id.highscore_act_info_text)).setTypeface(GameHelpers.getFont(this));
        ((TextView)findViewById(R.id.highscore_info_text)).setTypeface(GameHelpers.getFont(this));

        onSwipeTouchListener = new OnSwipeTouchListener(this){
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                finish();
            }
        };

        findViewById(R.id.main_highscore).setOnTouchListener(onSwipeTouchListener);
        recyclerView.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                finish();
            }

        });
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                if (swipeDir == ItemTouchHelper.LEFT) {
                    int position = viewHolder.getAdapterPosition();
                    mgr.remove(highScoreEntryList.get(position));
                    highScoreEntryList.remove(position);
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;

                    Paint p = new Paint();
                    int _16dp = (int) GameHelpers.convertDpToPixel(16f,getApplicationContext());

                    Bitmap b = BitmapFactory.decodeResource(getResources(),R.drawable.ic_delete_white_48dp);
                    int h = itemView.getBottom() - itemView.getTop();
                    int x = (h-b.getHeight())/2;

                    p.setColor(Color.RED);
                    // Draw Rect with varying left side, equal to the item's right side plus negative displacement dX
                    c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                            (float) itemView.getRight(), (float) itemView.getBottom(), p);
                    if (b.getWidth()+_16dp < -dX)
                        c.drawBitmap(b,
                                c.getWidth()-_16dp-b.getWidth(),
                                itemView.getTop() + x,p);


                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameHelpers.hideNavigations(this);
    }
}