package de.favo.game.snake;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface SnakeEvents{
    int UNDEFINED = 0;
    int VIEW_FULL = 1;
    int SNAKE_LEFT_SCREEN = 4;
    int SNAKE_WAS_DESTROYED = 6;
    int SNAKE_BIT_ITSELF = 7;
    void gameOver(int score,int reason);
    void snakeGetLonger(int score,int size);
}
