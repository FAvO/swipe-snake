package de.favo.game.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.Vector;

import static android.view.SurfaceHolder.*;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

@SuppressLint("ViewConstructor")
public class GameView extends SurfaceView implements Runnable, GameMotion, Callback{

    public volatile boolean gameIsPlaying = true;
    private Thread gameThread = null;

    private Snake game;
    private GameAttributes gameAttributes;
    private int objectSize = 128;

    //These objects will be used for drawing
    private Paint paint;
    private SurfaceHolder surfaceHolder;
    private int speed = 500;

    public GameView(Context context,GameAttributes attributes) {
        super(context);
        surfaceHolder = getHolder();
        paint = new Paint();
        surfaceHolder.addCallback(this);
        this.gameAttributes = attributes;
    }

    public GameAttributes getGameAttributes() {
        return gameAttributes;
    }

    @Override
    public void run() {
        while (gameIsPlaying) {
            update();
            draw(false);
            control();
        }
    }

    private void update() {
        game.goForward();
    }

    public Bitmap paint(boolean head){
        Bitmap bitmap = Bitmap.createBitmap(objectSize, objectSize, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        int padding = objectSize /20;
        int[] colors = {Color.BLUE, Color.RED, Color.GREEN};
        int round = gameAttributes.isMakeSnakeRound() ? 20 : 0;

        p.setStyle(Paint.Style.FILL);
        if (gameAttributes.isUseColorFullMode())
            p.setColor(colors[(game.getSnakeWithTail().size()+game.getShouldGrow()) % 3]);
        else
            p.setColor(gameAttributes.getForegroundColor());
        c.drawRoundRect(
                new RectF(padding, padding, objectSize -padding, objectSize -padding),
                round,round, p);

        if (head){
            p.setStyle(Paint.Style.FILL);
            p.setColor(Color.BLACK);
            int x1 = objectSize /4, x2 = (int) (objectSize *0.75f);
            int y2 = objectSize /4;
            int eye = 20;
            c.drawRoundRect(GameHelpers.getRectF(x1-eye/2, y2, eye,eye),5,5,p);
            c.drawRoundRect(GameHelpers.getRectF(x2-eye/2, y2, eye,eye),5,5,p);
        }
        return bitmap;
    }
    public Bitmap getGameScreen(boolean pause){
        Bitmap b = Bitmap.createBitmap(gameAttributes.getPx_width(), gameAttributes.getPx_height(),
                Bitmap.Config.ARGB_8888);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        Canvas c = new Canvas(b);
        Bitmap head,body;

        int alpha = pause ? 155 : 255;
        int round = gameAttributes.isMakeSnakeRound() ? 10 : 0;

        int  Xpadding = 0, Ypadding = 0,
                Xblocks = gameAttributes.getXBlockCount(),Yblocks = gameAttributes.getYBlockCount(),
                xBlockSize = gameAttributes.getBlockWidth(), yBlockSize = gameAttributes.getBlockHeight(),
                Xsize = gameAttributes.getPx_width(), Ysize = gameAttributes.getPx_height();
        Vector<SnakePart> tail = game.getSnakeWithTail();

        if (gameAttributes.isMakeSnakeRound()) {
            head = GameHelpers.makeTransparent(Bitmap.createScaledBitmap(paint(true),
                    gameAttributes.getBlockWidth(),
                    gameAttributes.getBlockHeight(), false), alpha);
            body = GameHelpers.makeTransparent(Bitmap.createScaledBitmap(paint(false),
                    gameAttributes.getBlockWidth(),
                    gameAttributes.getBlockHeight(), false), alpha);
        }else{
            head = GameHelpers.makeTransparent(Bitmap.createScaledBitmap(paint(true),
                    gameAttributes.getBlockWidth(),
                    gameAttributes.getBlockHeight(), false), alpha);
            body = GameHelpers.makeTransparent(Bitmap.createScaledBitmap(paint(false),
                    gameAttributes.getBlockWidth(),
                    gameAttributes.getBlockHeight(), false), alpha);
        }

        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setColor(gameAttributes.getBackgroundColor());
        c.drawRect(new Rect(0,0,Xsize,Ysize),p);
        {
            // Determinate padding
            Xpadding = ( Xsize - xBlockSize * Xblocks ) / 2;
            Ypadding = ( Ysize - yBlockSize * Yblocks ) / 2;
        }
        {
            // draw walls
            Vector<WallBlock> walls = game.getWallBlocks();
            p.setColor(GameHelpers.getColorWithAlpha(Color.WHITE,alpha));
            p.setStyle(Paint.Style.FILL_AND_STROKE);
            for (WallBlock w : walls)
                c.drawRoundRect(
                        GameHelpers.getRectF(
                                w.getX() * xBlockSize + Xpadding,
                                w.getY() * yBlockSize + Ypadding,
                                xBlockSize,yBlockSize),
                        round,round,p);
        }
        {
            //draw events
            for (SnakeEvent se : new Vector<>(game.getEvents()))
                c.drawBitmap(GameHelpers.makeTransparent(
                        Bitmap.createScaledBitmap(se.getMySelf(),xBlockSize,yBlockSize,false),alpha)
                        ,se.getX()*xBlockSize+Xpadding,se.getY()*yBlockSize+Ypadding,p);
        }
        {
            // draw grid
            if (gameAttributes.isShowGrid()) {
                int col = Color.parseColor("#333333");
                p.setColor(col);
                p.setStyle(Paint.Style.STROKE);
                for (int y = 0; y < gameAttributes.getYBlockCount(); y++) {
                    c.drawLine(Xpadding, (y * yBlockSize) + Ypadding,
                            gameAttributes.getPx_width() - Xpadding, (y * yBlockSize) + Ypadding, p);
                }
                for (int x = 0; x < gameAttributes.getXBlockCount(); x++) {
                    c.drawLine((x * xBlockSize + Xpadding), Ypadding,
                            x * xBlockSize + Xpadding, gameAttributes.getPx_height() - Ypadding, p);
                }
            }
        }

        {
            // Draw the Snake
            c.drawBitmap(GameHelpers.rotateBitmap(head,tail.lastElement().getOrientation()),
                    (tail.lastElement().getX()) * xBlockSize + Xpadding,
                    (tail.lastElement().getY()) * yBlockSize + Ypadding,p);
            for (int v3 = 0; v3 < tail.size()-1; v3++)
                c.drawBitmap(GameHelpers.rotateBitmap(body,tail.elementAt(v3).getOrientation()),
                        (tail.elementAt(v3).getX()) * xBlockSize + Xpadding,
                        (tail.elementAt(v3).getY()) * yBlockSize + Ypadding,p);
        }
        {
            //draw border
            RectF r = new RectF();
            r.set(Xpadding,Ypadding,Xsize - Xpadding, Ysize - Ypadding);
            p.setColor(GameHelpers.getColorWithAlpha(gameAttributes.getForegroundColor(),alpha));
            p.setStrokeWidth(2);
            p.setStyle(Paint.Style.STROKE);
            c.drawRoundRect(r,round,round,p);
        }
        return b;
    }
    private void draw(boolean conPause) {
        //checking if surface is valid
        if (surfaceHolder.getSurface().isValid()) {
            //locking the canvas
            Canvas canvas = surfaceHolder.lockCanvas();
            //drawing a background color for canvas
            Bitmap bitmap = getGameScreen(conPause);
            if ((bitmap == null) || (canvas == null))
                return;
            canvas.drawBitmap(bitmap,0,0,paint);

            // draw text
            if (conPause){
                String pause = getContext().getString(R.string.app_game_pause);
                Typeface typeface = GameHelpers.getBoldFont(getContext());
                int txtpadding = 100,
                        txtsize = GameHelpers.determineMaxTextSize(pause,typeface,getWidth()-txtpadding);
                paint.setColor(Color.GRAY);
                paint.setTypeface(typeface);
                paint.setTextSize(txtsize);
                canvas.drawText(pause,txtpadding/2,getHeight()/2,paint);
                paint.setColor(Color.argb(50,0,0,0));
                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawRect(0,0,canvas.getWidth(),canvas.getHeight(),paint);
                paint.setColor(Color.BLACK);
            }
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() {
        try {
            gameThread.sleep(speed);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        gameIsPlaying = false;
        try {
            gameThread.join();
            draw(true);
        } catch (InterruptedException e) {
            Log.i("Snake","Interrupted");
        }
    }
    public void stop(){
        gameIsPlaying = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.i("Snake","Interrupted");
        }
    }

    public void resume() {
        gameIsPlaying = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //initializing player object
        gameAttributes.setXSize(getWidth());
        gameAttributes.setYSize(getHeight());
        speed = gameAttributes.getSpeed();
        game = new Snake(gameAttributes);
        if (gameIsPlaying)
            resume();
        else {
            draw(true);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }
    public void surfaceDestroyed(SurfaceHolder holder) {
        pause();
    }

    public GameMotion getListener(){
        return this;
    }

    @Override
    public void goNorth() {
        if (game.getOrientation() != Snake.SOUTH)
            game.changeDirection(Snake.NORTH);
    }

    @Override
    public void goSouth() {
        if (game.getOrientation() != Snake.NORTH)
            game.changeDirection(Snake.SOUTH);
    }

    @Override
    public void goWest() {
        if (game.getOrientation() != Snake.EAST)
            game.changeDirection(Snake.WEST);
    }

    @Override
    public void goEast() {
        if (game.getOrientation() != Snake.WEST)
            game.changeDirection(Snake.EAST);
    }
}