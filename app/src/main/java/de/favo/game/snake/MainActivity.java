package de.favo.game.snake;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class MainActivity extends Activity {
    private TextView scoreTextView, infoTextView;
    private GameView gameView;
    private GameMotion gameMotions;
    private ImageButton pauseImageButton;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scoreTextView = findViewById(R.id.score);
        pauseImageButton = findViewById(R.id.pause);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        infoTextView = findViewById(R.id.main_info);

        GameAttributes attr = new GameAttributes();

        attr.setUseColorFullMode(sharedPreferences.getBoolean("color_scheme",false));
        attr.setBlockHeight(sharedPreferences.getInt("block_size",10));
        attr.setBlockWidth(sharedPreferences.getInt("block_size",10));
        attr.setForegroundColor(getResources().getColor(R.color.colorDarkFg));
        attr.setBackgroundColor(getResources().getColor(R.color.colorDarkBg));
        attr.setSpeed(sharedPreferences.getInt("speed",500));
        attr.setWall(sharedPreferences.getInt("walls",0));
        attr.setMakeSnakeRound(sharedPreferences.getBoolean("round",false));
        attr.setShowGrid(sharedPreferences.getBoolean("show_grid",true));
        attr.setEvents(new SnakeEvents() {

            @Override
            public void gameOver(final int score, final int reason) {
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                if (v != null && sharedPreferences.getBoolean("vibrate",true)) {
                    v.vibrate(1000);
                }
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Handler handler = new Handler(Looper.getMainLooper());
                        gameView.stop();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(MainActivity.this,ResultActivity.class);
                                i.putExtra("scoreTextView",score);
                                i.putExtra("reason",reason);
                                i.putExtra("speed", gameView.getGameAttributes().getSpeed());
                                MainActivity.this.startActivityForResult(i,300);
                            }
                        });}
                });
            }
            @Override
            public void snakeGetLonger(final int s,int length) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        scoreTextView.setText(s+"");
                    }
                });
            }
        });

        gameView = new GameView(this,attr);
        ((LinearLayout) findViewById(R.id.game)).addView(gameView);
        gameMotions = gameView.getListener();
        pauseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            pause(gameView.gameIsPlaying);
            }
        });
        scoreTextView.setTypeface(GameHelpers.getFont(this));
        ((TextView)findViewById(R.id.view_score)).setTypeface(GameHelpers.getFont(this));
        findViewById(R.id.game_main).setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                if (gameView.gameIsPlaying)
                    gameMotions.goSouth();
            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                if (gameView.gameIsPlaying)
                    gameMotions.goWest();
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                if (gameView.gameIsPlaying)
                    gameMotions.goEast();
                if (infoTextView.getVisibility() == View.VISIBLE)
                    finish();
            }

            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                if (gameView.gameIsPlaying)
                    gameMotions.goNorth();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 300){
            if (resultCode == Activity.RESULT_OK)
                finish();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // 21 is the LEFT arrow key!
        if (!gameView.gameIsPlaying && (keyCode == 21 || keyCode == KeyEvent.KEYCODE_ESCAPE || keyCode == KeyEvent.KEYCODE_DEL))
            finishNow();

        if (gameView.gameIsPlaying){
            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT)
                gameView.goWest();
            if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)
                gameView.goEast();
            if (keyCode == KeyEvent.KEYCODE_DPAD_UP)
                gameView.goNorth();
            if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN)
                gameView.goSouth();
            if (keyCode == KeyEvent.KEYCODE_SPACE) {
                pause(true);
                return super.onKeyUp(keyCode,event);
            }
        }

        if (!gameView.gameIsPlaying && keyCode == KeyEvent.KEYCODE_SPACE)
            pause(false);

        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameHelpers.hideNavigations(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pause(true);
    }

    @Override
    public void onBackPressed() {
        if (gameView.gameIsPlaying)
            pause(true);
        else
            finishNow();
    }

    private void finishNow(){
        finish();
    }

    private void pause(boolean p){
        if (p)
            gameView.pause();
        else
            gameView.resume();
        pauseImageButton.setImageResource( p ? R.drawable.ic_pause_circle_filled_24dp : R.drawable.ic_pause_24dp);
        infoTextView.setVisibility(p ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
