package de.favo.game.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class HighScoreManager {
    private Vector<HighScoreEntry> entries;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    HighScoreManager(Context cxt) {
        sharedPreferences = cxt.getSharedPreferences(
                "local_High_Score_in_" + cxt.getPackageName(), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        entries = new Vector<>();
        loadData();
    }

    void addEntry(HighScoreEntry entry) {
        entries.add(entry);
    }

    Vector<HighScoreEntry> getEntriesSortedByDate() {
        Vector<HighScoreEntry> newEntries = new Vector<>(entries);
        Collections.sort(newEntries, new Comparator<HighScoreEntry>() {
            @Override
            public int compare(HighScoreEntry o1, HighScoreEntry o2) {
                if (o1.getDate().equals(o2.getDate()))
                    return 0;
                return o1.getDate().after(o2.getDate()) ? -1 : 1;
            }
        });
        return newEntries;
    }

    void remove(HighScoreEntry entry){
        entries.remove(entry);
        save();
    }

    Vector<HighScoreEntry> getEntriesSortedByBest() {
        Vector<HighScoreEntry> newEntries = new Vector<>(entries);
        Collections.sort(newEntries, new Comparator<HighScoreEntry>() {
            @Override
            public int compare(HighScoreEntry o1, HighScoreEntry o2) {
                if (o1.getScore() == o2.getScore())
                    return o1.getDate().after(o2.getDate()) ? -1 : 1;
                if (o1.getScore() > o2.getScore())
                    return -1;
                if (o1.getScore() < o2.getScore())
                    return 1;
                return 0;
            }
        });
        return newEntries;
    }

    public HighScoreEntry getBest() {
        HighScoreEntry best = null;
        for (HighScoreEntry entry : entries) {
            if (best == null)
                best = entry;
            else if (entry.getScore() > best.getScore())
                best = entry;
            else if (entry.getScore() == best.getScore())
                if (entry.getDate().after(best.getDate()))
                    best = entry;
        }
        return best;
    }

    private void loadData() {
        int amountOfEntries = sharedPreferences.getInt("amount_of_entries", 0);
        for (int x = 0; x < amountOfEntries; x++) {
            entries.add(
                    HighScoreEntry.fromString(
                            sharedPreferences.getString("entry_" + x, "")));
        }
    }

    void save() {
        editor.putInt("amount_of_entries", entries.size());
        for (int i = 0; i < entries.size(); i++)
            editor.putString("entry_" + i, entries.elementAt(i).toString());
        editor.apply();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (HighScoreEntry entry: entries) {
            sb.append(entry.getName());
            sb.append(":");
            sb.append(entry.getScore());
            sb.append(":");
            sb.append(entry.getSpeed());
            sb.append(":");
            sb.append(entry.getDate().toLocaleString());
            sb.append("\n");
            sb.append("\n");
        }
        return sb.toString();
    }

    public static class HighScoreEntry {
        private String name;
        private int score;
        private int reason;
        private long speed;
        private Date date;

        public HighScoreEntry(String name, int score, int reason, long speed) {
            this.name = name;
            this.score = score;
            this.speed = speed;
            this.reason = reason;
            this.date = new Date(System.currentTimeMillis());
        }

        HighScoreEntry(String name, int score, int reason, long speed, Date date) {
            this.name = name;
            this.score = score;
            this.speed = speed;
            this.date = date;
            this.reason = reason;
        }

        long getSpeed() {
            return speed;
        }

        int getScore() {
            return score;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name + "\n" +
                    score + "\n" +
                    reason + "\n" +
                    date.getTime() + "\n" +
                    speed;
        }

        static HighScoreEntry fromString(String str) {
            String[] entries = str.split("\n");
            return new HighScoreEntry(
                    entries[0],
                    Integer.parseInt(entries[1]),
                    Integer.parseInt(entries[2]),
                    Long.parseLong(entries[4]),
                    new Date(Long.parseLong(entries[3])));
        }

        Date getDate() {
            return date;
        }

        public int getReason() {
            return reason;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HighScoreEntry entry = (HighScoreEntry) o;

            if (score != entry.score) return false;
            if (reason != entry.reason) return false;
            if (speed != entry.speed) return false;
            if (name != null ? !name.equals(entry.name) : entry.name != null) return false;
            return date != null ? date.equals(entry.date) : entry.date == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + score;
            result = 31 * result + reason;
            result = 31 * result + (int) (speed ^ (speed >>> 32));
            result = 31 * result + (date != null ? date.hashCode() : 0);
            return result;
        }
    }
}
