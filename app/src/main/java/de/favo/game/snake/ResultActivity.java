package de.favo.game.snake;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;
import java.util.Locale;
import java.util.Vector;
/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class ResultActivity extends Activity {
    private HighScoreManager highscoreManager;
    private EditText name;
    private int scoreAsInt,reason,speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View decorView = getWindow().getDecorView();
        super.onCreate(savedInstanceState);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        decorView.setSystemUiVisibility(uiOptions);
        getWindow().setBackgroundDrawableResource(R.drawable.borderedbg);
        setContentView(R.layout.activity_result);
        setFinishOnTouchOutside(false);

        highscoreManager = new HighScoreManager(this);

        scoreAsInt = getIntent().getIntExtra("score",0);
        reason = getIntent().getIntExtra("reason", SnakeEvents.UNDEFINED);
        speed = getIntent().getIntExtra("speed",0);

        Button ok = findViewById(R.id.go_dialog_ok);
        TextView score = (findViewById(R.id.go_dialog_your_score));
        name = findViewById(R.id.go_dialog_your_text);
        TextView best = findViewById(R.id.go_dialog_you_re_the_best);

        ((TextView)findViewById(R.id.go_dialog_message)).setTypeface(GameHelpers.getBoldFont(this));
        ((TextView)findViewById(R.id.go_dialog_your_score_text)).setTypeface(GameHelpers.getFont(this));
        ((TextView)findViewById(R.id.go_dialog_your_text_hint)).setTypeface(GameHelpers.getFont(this));

        ok.setTypeface(GameHelpers.getFont(this));
        name.setTypeface(GameHelpers.getLightFont(this));
        best.setTypeface(GameHelpers.getBoldFont(this));
        score.setTypeface(GameHelpers.getFont(this));
        score.setText(String.format(Locale.getDefault(),"%d", scoreAsInt));

        Vector<HighScoreManager.HighScoreEntry> bestE = highscoreManager.getEntriesSortedByBest();
        Vector<HighScoreManager.HighScoreEntry> dateE = highscoreManager.getEntriesSortedByDate();
        if ((bestE != null) && (bestE.size() > 0))
            if (bestE.elementAt(0).getScore() >= scoreAsInt)
                best.setVisibility(View.GONE);
        if ((dateE != null) && (dateE.size() > 0))
            name.setText(dateE.elementAt(0).getName());
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                highscoreManager.addEntry(
                        new HighScoreManager.HighScoreEntry(
                                name.getText().toString(),
                                scoreAsInt,reason,speed,new Date(System.currentTimeMillis())));
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        highscoreManager.save();
    }

    @Override
    public void onBackPressed() {
    }
}
