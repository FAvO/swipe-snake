package de.favo.game.snake;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GameHelpers {
    static RectF getRectF(int x, int y, int width, int height){
        return new RectF(x,y,x+width,y+height);
    }

    static Bitmap makeTransparent(Bitmap src, int alpha) {
        if (alpha == 255)
            return src;
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap transBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(transBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        // config paint
        final Paint paint = new Paint();
        paint.setAlpha(alpha);
        canvas.drawBitmap(src, 0, 0, paint);
        return transBitmap;
    }

    static int getColorWithAlpha(int color, int alpha){
        if (alpha == 255)
            return color;
        if (alpha == 0)
            return Color.TRANSPARENT;
        return Color.argb(alpha,Color.red(color),Color.green(color),Color.blue(color));
    }

    static Bitmap rotateBitmap(Bitmap source, float angle) {
        if (angle == 0)
            return source;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    static int determineMaxTextSize(String str, Typeface font, float maxWidth) {
        int size = 0;
        Paint paint = new Paint();
        paint.setTypeface(font);
        do {
            paint.setTextSize(++ size);
        } while(paint.measureText(str) < maxWidth);

        return size;
    }

    static Typeface getBoldFont(Context cxt){
        return Typeface.createFromAsset(
                cxt.getAssets(),"fonts/Comfortaa/Comfortaa-Bold.ttf");
    }

    static Typeface getFont(Context cxt){
        return Typeface.createFromAsset(
                cxt.getAssets(),"fonts/Comfortaa/Comfortaa-Regular.ttf");
    }

    static Typeface getLightFont(Context cxt){
        return Typeface.createFromAsset(
                cxt.getAssets(),"fonts/Comfortaa/Comfortaa-Light.ttf");
    }

    public static Bitmap scaleBitmapWithFactor(Bitmap b, float factor){
        return Bitmap.createScaledBitmap(b,
                (int)(b.getWidth()*factor),
                (int)(b.getHeight()*factor),true);
    }

    static void hideNavigations(Activity context) {
        View decorView = context.getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    static void changeTTF(Context c, View v) {
        if (v instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) v).getChildCount(); i++) {
                changeTTF(c,((ViewGroup) v).getChildAt(i));
            }
        } else {
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(GameHelpers.getFont(c));
            }
        }
    }

}


